Context Everywhere
============================

This module disables the core Block UI, allowing the Context module to have
complete control over block placement on the site.

Once enabled, a new 'Disable core Block UI' setting will be available at
/admin/structure/context/settings. It is enabled by default.

This module does not prevent administrators from using contextual links
to configure blocks in other ways.
